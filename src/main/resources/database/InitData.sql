insert into students(student_id, name, course, lastname)
VALUES (1, 'Evgeniy', 1, 'Shalchinov'),
       (2, 'Bernard', 1, 'Kelso'),
       ( 3, 'Sergey', 1, 'Nikovaev');

insert into teachers (teacher_id, name, lastname, subject) VALUES
(1, 'Alexey', 'Zakharov', 'Philosophy'),
(2, 'Denis', 'Kefirov', 'Math'),
(3, 'Semen', 'Krishovnik', 'Physics');

insert into students_teachers(student_id, teacher_id) VALUES
(1, 1),
(1, 2),
(1, 3),
(2,1),
(2, 3),
(3, 1),
(3, 2);

Select s.student_id, s.name, s.course, s.lastname from students_teachers inner join students s on students_teachers.student_id = s.student_id
inner join teachers t on students_teachers.teacher_id = t.teacher_id where t.teacher_id = 2;

insert into students_teachers(teacher_id, student_id) VALUES
(1,1);



Select t.name, t.lastname from students_teachers inner join students s on students_teachers.student_id = s.student_id
                                                                         inner join teachers t on students_teachers.teacher_id = t.teacher_id where s.student_id = 1;
delete from students_teachers where teacher_id= 2 and student_id= 1;