package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "teachers")
public class Teacher {
    @Id
    private long teacher_id;
    private String name;
    private String lastname;
    private String subject;

    public Teacher() {
    }


    public Teacher(long teacher_id, String name, String lastname, String subject) {
        this.teacher_id = teacher_id;
        this.name = name;
        this.lastname = lastname;
        this.subject = subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return teacher_id == teacher.teacher_id && Objects.equals(name, teacher.name) && Objects.equals(lastname, teacher.lastname) && Objects.equals(subject, teacher.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacher_id, name, lastname, subject);
    }

    public void setTeacher_id(long teacher_id) {
        this.teacher_id = teacher_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public long getTeacher_id() {
        return teacher_id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getSubject() {
        return subject;
    }
}
