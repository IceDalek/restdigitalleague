package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.util.Objects;
import java.io.Serializable;

@Entity
@Table(name = "students_teachers")
@IdClass(StudentsTeachers.class)
public class StudentsTeachers implements Serializable {
    @Id
    private long student_id;
    @Id
    private long teacher_id;

    public StudentsTeachers() {
    }

    public StudentsTeachers(long student_id, long teacher_id) {
        this.student_id = student_id;
        this.teacher_id = teacher_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentsTeachers that = (StudentsTeachers) o;
        return student_id == that.student_id && teacher_id == that.teacher_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(student_id, teacher_id);
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public void setTeacher_id(long teacher_id) {
        this.teacher_id = teacher_id;
    }

    public long getStudent_id() {
        return student_id;
    }

    public long getTeacher_id() {
        return teacher_id;
    }
}
