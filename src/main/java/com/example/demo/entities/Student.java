package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "students")
public class Student {
    @Id
    private long student_id;
    private String name;
    private int course;
    private String lastname;

    public Student() {
    }

    public Student(long student_id, String name, int course, String lastname) {
        this.student_id = student_id;
        this.name = name;
        this.course = course;
        this.lastname = lastname;
    }

    public long getStudent_id() {
        return student_id;
    }

    public String getName() {
        return name;
    }

    public int getCourse() {
        return course;
    }

    public String getLastname() {
        return lastname;
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "Student{" +
                "student_id=" + student_id +
                ", name='" + name + '\'' +
                ", course=" + course +
                ", lastname='" + lastname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return student_id == student.student_id && course == student.course && Objects.equals(name, student.name) && Objects.equals(lastname, student.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student_id, name, course, lastname);
    }
}
