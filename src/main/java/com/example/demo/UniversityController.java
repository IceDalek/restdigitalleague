package com.example.demo;

import com.example.demo.entities.Student;
import com.example.demo.entities.StudentsTeachers;
import com.example.demo.entities.Teacher;
import com.example.demo.repository.StudentsRepository;
import com.example.demo.repository.StudentsTeachersRepository;
import com.example.demo.repository.TeachersRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class UniversityController {
    private final StudentsRepository studentsRepository;
    private final TeachersRepository teachersRepository;
    private final StudentsTeachersRepository studentsTeachersRepository;

    public UniversityController(StudentsRepository studentsRepository,
                                TeachersRepository teachersRepository,
                                StudentsTeachersRepository studentsTeachersRepository) {
        this.studentsRepository = studentsRepository;
        this.teachersRepository = teachersRepository;
        this.studentsTeachersRepository = studentsTeachersRepository;
    }

    //return all students
    @GetMapping("/students")
    List<Student> allStudents() {
        return (List<Student>) studentsRepository.findAll();
    }


    //add new student
    @PostMapping("/students")
    Student newStudent(@RequestBody Student newStudent) {
        return studentsRepository.save(newStudent);
    }

    //get student by id
    @GetMapping("/students/{id}")
    Student oneStudent(@PathVariable Long id) {
        return studentsRepository.findById(id).
                orElseThrow(NoSuchElementException::new);
    }

    @DeleteMapping("/students/{id}")
    void deleteStudent(@PathVariable Long id) {
        studentsRepository.deleteById(id);
    }

    @GetMapping("/students/get_list_of_teachers/{id}")
    List<Teacher> getListOfTeachers(@PathVariable Long id) {
        return teachersRepository.getListOfTeachers(id);
    }

    @GetMapping("/teachers")
    List<Teacher> allTeachers() {
        return (List<Teacher>) teachersRepository.findAll();
    }

    //get teacher by id
    @GetMapping("/teachers/{id}")
    Teacher oneTeacher(@PathVariable Long id) {
        return teachersRepository.findById(id).
                orElseThrow(NoSuchElementException::new);
    }

    @PostMapping("/teachers")
    Teacher newTeacher(@RequestBody Teacher newTeacher) {
        return teachersRepository.save(newTeacher);
    }

    @DeleteMapping("/teachers/{id}")
    void deleteTeacher(@PathVariable Long id) {
        teachersRepository.deleteById(id);
    }

    //get all students of teacher
    @GetMapping("/teachers/get_list_of_students/{id}")
    List<Student> getListOfStudents(@PathVariable Long id) {
        return studentsRepository.getListOfStudents(id);
    }

    @PostMapping("/students/bind/")
    void bindStudentToTeacher(@RequestBody StudentsTeachers studentsTeachers) {
        studentsTeachersRepository.save(studentsTeachers);
    }

    @PostMapping("/students/bind/{studentId}/{teacherId}")
    void bindStudentToTeacher1(@PathVariable Long studentId, @PathVariable Long teacherId) {
        studentsTeachersRepository.bindStudentToTeacher(studentId, teacherId);
    }

    @DeleteMapping("/students/unbound/{studentId}/{teacherId}")
    void unboundStudentFromTeacher(@PathVariable Long studentId, @PathVariable Long teacherId) {
        System.out.println(studentId);
        System.out.println(teacherId);
        studentsTeachersRepository.unboundStudentFromTeacher(studentId, teacherId);
    }

}
