package com.example.demo.Service;

import java.util.List;

import com.example.demo.entities.Student;

public interface StudentsService {
    List<Student> findAll();

    List<Student> getListOfStudents(Long id);
}
