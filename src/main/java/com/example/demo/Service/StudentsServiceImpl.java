package com.example.demo.Service;

import com.example.demo.entities.Student;
import com.example.demo.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentsServiceImpl implements StudentsService {

    @Autowired
    private StudentsRepository repository;

    @Override
    public List<Student> findAll() {
        var students = (List<Student>) repository.findAll();
        return students;
    }

    @Override
    public List<Student> getListOfStudents(Long id) {
        return repository.getListOfStudents(id);
    }

}
