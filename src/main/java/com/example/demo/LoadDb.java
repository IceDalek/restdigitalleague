package com.example.demo;

import com.example.demo.entities.Student;
import com.example.demo.repository.StudentsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.CommandLineRunner;
/*
@Configuration
public class LoadDb {
    private static final Logger log = LoggerFactory.getLogger(LoadDb.class);
    @Bean
    CommandLineRunner initDb(StudentsRepository repository){
        return args -> {
            log.info("Load into Db" + repository.save(new Student(7, "Evgen", 1, "Kachan")));
        };
    }
}
*/