package com.example.demo.repository;

import com.example.demo.entities.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentsRepository extends CrudRepository<Student, Long> {
    @Query(
            value = "Select s.student_id, s.name, s.course, s.lastname from students_teachers inner join students s on students_teachers.student_id = s.student_id\n" +
                    "inner join teachers t on students_teachers.teacher_id = t.teacher_id where t.teacher_id= :id",
            nativeQuery = true)
    List<Student> getListOfStudents(@Param("id") Long id);
}
