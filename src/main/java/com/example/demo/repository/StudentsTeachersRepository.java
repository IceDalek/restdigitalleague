package com.example.demo.repository;

import com.example.demo.entities.StudentsTeachers;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;

@Repository
public interface StudentsTeachersRepository extends CrudRepository<StudentsTeachers, Long> {
    @Modifying
    @Transactional
    @Query(
            value = "insert into students_teachers(teacher_id, student_id) VALUES\n" +
                    "(:studentId, :teacherId)",
            nativeQuery = true)
    void bindStudentToTeacher(@Param("studentId") Long studentId, @Param("teacherId") Long teacherId);

    @Modifying
    @Transactional
    @Query(
            value = "delete from students_teachers where teacher_id=:ti and student_id=:si",
            nativeQuery = true)
    void unboundStudentFromTeacher(@Param("ti") Long ti, @Param("si") Long si);
}
