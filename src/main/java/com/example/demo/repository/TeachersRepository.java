package com.example.demo.repository;

import com.example.demo.entities.Student;
import com.example.demo.entities.Teacher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeachersRepository extends CrudRepository<Teacher, Long> {
    @Query(
            value = "Select t.teacher_id, t.name, t.lastname,t.subject from students_teachers inner join students s on students_teachers.student_id = s.student_id\n" +
                    "inner join teachers t on students_teachers.teacher_id = t.teacher_id where s.student_id= :id",
            nativeQuery = true)
    List<Teacher> getListOfTeachers(@Param("id") Long id);
}
